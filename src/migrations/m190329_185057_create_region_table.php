<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%region}}`.
 */
class m190329_185057_create_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'active' => $this->integer()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');


        $this->addCommentOnColumn('{{%region}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%region}}','name', 'Имя региона');
        $this->addCommentOnColumn('{{%region}}','active', 'Активность записи');

        $this->addCommentOnTable('{{%region}}','Список регионов');


        $region = array(
            array('id' => '1','name' => 'Днепропетровская','active' => '1'),
            array('id' => '2','name' => 'Одесская','active' => '1')
          );

          foreach ($region as $v) {
            $this->insert('region', $v);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%region}}');
    }
}
