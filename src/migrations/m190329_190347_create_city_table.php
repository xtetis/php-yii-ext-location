<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%city}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%region}}`
 */
class m190329_190347_create_city_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'id_region' => $this->integer()->notNull(),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'active' => $this->integer()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_region`
        $this->createIndex(
            '{{%idx-city-id_region}}',
            '{{%city}}',
            'id_region'
        );

        // add foreign key for table `{{%region}}`
        $this->addForeignKey(
            '{{%fk-city-id_region}}',
            '{{%city}}',
            'id_region',
            '{{%region}}',
            'id',
            'CASCADE'
        );


        $this->addCommentOnColumn('{{%city}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%city}}','id_region', 'Регион. Ссылка на таблицу region');
        $this->addCommentOnColumn('{{%city}}','name', 'Название города');
        $this->addCommentOnColumn('{{%city}}','active', 'Активность записи');

        $this->addCommentOnTable('{{%city}}','Список городов');



        $city = array(
            array('id' => '1','id_region' => '1','name' => 'Кривой Рог','active' => '1'),
            array('id' => '2','id_region' => '2','name' => 'Одесса','active' => '1')
          );

          foreach ($city as $v) {
            $this->insert('city', $v);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%region}}`
        $this->dropForeignKey(
            '{{%fk-city-id_region}}',
            '{{%city}}'
        );

        // drops index for column `id_region`
        $this->dropIndex(
            '{{%idx-city-id_region}}',
            '{{%city}}'
        );

        $this->dropTable('{{%city}}');
    }
}
