<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%district}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%city}}`
 */
class m190329_193504_create_district_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%district}}', [
            'id' => $this->primaryKey(),
            'id_city' => $this->integer()->notNull(),
            'name' => $this->string(200)->append('CHARACTER SET utf8 COLLATE utf8_general_ci'),
            'active' => $this->integer()->defaultValue(1),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // creates index for column `id_city`
        $this->createIndex(
            '{{%idx-district-id_city}}',
            '{{%district}}',
            'id_city'
        );

        // add foreign key for table `{{%city}}`
        $this->addForeignKey(
            '{{%fk-district-id_city}}',
            '{{%district}}',
            'id_city',
            '{{%city}}',
            'id',
            'CASCADE'
        );

        $this->addCommentOnColumn('{{%district}}','id', 'Первичный ключ');
        $this->addCommentOnColumn('{{%district}}','id_city', 'Город. Ссылка на таблицу city');
        $this->addCommentOnColumn('{{%district}}','name', 'Название района');
        $this->addCommentOnColumn('{{%district}}','active', 'Активность записи');

        $this->addCommentOnTable('{{%district}}','Список районов города');


        $district = array(
            array('id' => '1','id_city' => '1','name' => 'Металлургический','active' => '1'),
            array('id' => '2','id_city' => '2','name' => 'Малиновский','active' => '1')
          );

          foreach ($district as $v) {
            $this->insert('district', $v);
        }
          
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%city}}`
        $this->dropForeignKey(
            '{{%fk-district-id_city}}',
            '{{%district}}'
        );

        // drops index for column `id_city`
        $this->dropIndex(
            '{{%idx-district-id_city}}',
            '{{%district}}'
        );

        $this->dropTable('{{%district}}');
    }
}
