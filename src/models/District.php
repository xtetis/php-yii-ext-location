<?php

namespace xtetis\location\models;

use Yii;

/**
 * This is the model class for table "{{%district}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_city Город. Ссылка на таблицу city
 * @property string $name Название района
 * @property int $active Активность записи
 *
 * @property City $city
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%district}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_city'], 'required'],
            [['id_city', 'active'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['id_city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_city' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_city' => 'Город. Ссылка на таблицу city',
            'name' => 'Название района',
            'active' => 'Активность записи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'id_city'])->inverseOf('districts');
    }
}
