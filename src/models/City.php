<?php

namespace xtetis\location\models;

use Yii;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property int $id Первичный ключ
 * @property int $id_region Регион. Ссылка на таблицу region
 * @property string $name Название города
 * @property int $active Активность записи
 *
 * @property Region $region
 * @property District[] $districts
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_region'], 'required'],
            [['id_region', 'active'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['id_region'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['id_region' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_region' => 'Регион. Ссылка на таблицу region',
            'name' => 'Название города',
            'active' => 'Активность записи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'id_region'])->inverseOf('cities');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['id_city' => 'id'])->inverseOf('city');
    }
}
