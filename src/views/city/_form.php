<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use xtetis\location\models\Region;


/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'id_region')->textInput() ?>

    <?php echo $form->field($model, 'id_region')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Region::find()->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите регион'],
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]);
    ?>



    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->dropDownList([
    '1' => 'Активный',
    '0' => 'Отключен',
    ]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
