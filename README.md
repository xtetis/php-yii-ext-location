Module to manage location
=========================
Module to manage countries, regions, cities and districts

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Edit your composer.json

```
    "repositories": [
...
        {
            "type": "git",
            "url": "https://xtetis@bitbucket.org/xtetis/yii2-location.git"
        }
...
    ]
```

Either run

```
php composer.phar require --prefer-dist xtetis/yii2-location "dev-master"
```

or add

```
"xtetis/yii2-location":"dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
    'modules' => [
...
        'location' => [
            'class' => 'xtetis\location\Module',
            'as access' => [
                'class' => yii2mod\rbac\filters\AccessControl::class,
            ],
        ],
...
    ],
```


Migrations
-----
```
    ./yii migrate --migrationPath=@vendor/xtetis/yii2-location/migrations
```



# Модуль для управления даннми о географическом положении
Составляет из себя модуль для работы со странами, регионами, городами и районами города.
В составе:
- Модели для достаупа к нужным таблицам
- Модуль для управления данными в базе
- Миграция для создания необходимых таблиц в базе